<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tenistas".
 *
 * @property int $id
 * @property string $nombre
 * @property string $correo
 * @property int $activo
 * @property string $fechaBaja
 * @property string $fechaNacimiento
 * @property int $altura
 * @property int $peso
 * @property int $idnacion
 * @property int $edad
 *
 * @property Naciones $nacion
 */
class Tenistas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tenistas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['activo', 'altura', 'peso', 'idnacion', 'edad'], 'integer'],
            [['fechaBaja', 'fechaNacimiento'], 'safe'],
            [['nombre', 'correo'], 'string', 'max' => 20],
            [['idnacion'], 'exist', 'skipOnError' => true, 'targetClass' => Naciones::className(), 'targetAttribute' => ['idnacion' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'correo' => 'Correo',
            'activo' => 'Activo',
            'fechaBaja' => 'Fecha Baja',
            'fechaNacimiento' => 'Fecha Nacimiento',
            'altura' => 'Altura',
            'peso' => 'Peso',
            'idnacion' => 'Idnacion',
            'edad' => 'Edad',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNacion()
    {
        return $this->hasOne(Naciones::className(), ['id' => 'idnacion']);
    }
}
