<?php
use yii\helpers\Html
?>

<ul>
    <li>
<?= Html::a('Consulta 1', ['tenistas/consulta1'], ['class' => 'profile-link']) ?>
    </li>
    <li>
<?= Html::a('Consulta 2', ['tenistas/consulta2'], ['class' => 'profile-link']) ?>
    </li>
    <li>
<?= Html::a('Consulta 3', ['tenistas/consulta3'], ['class' => 'profile-link']) ?>
    </li>
    <li>
<?= Html::a('Consulta 4', ['tenistas/consulta4'], ['class' => 'profile-link']) ?>
    </li>
</ul>