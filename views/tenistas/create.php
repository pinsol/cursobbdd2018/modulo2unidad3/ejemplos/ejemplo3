<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\tenistas */

$this->title = 'Crear Tenista';
$this->params['breadcrumbs'][] = ['label' => 'Tenistas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tenistas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
