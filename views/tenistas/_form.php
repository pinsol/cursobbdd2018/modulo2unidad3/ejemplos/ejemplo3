<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Tenistas */
/* @var $form ActiveForm */
?>
<div class="Tenistas-_form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'activo') ?>
        <?= $form->field($model, 'altura') ?>
        <?= $form->field($model, 'peso') ?>
        <?= $form->field($model, 'idnacion') ?>
        <?= $form->field($model, 'edad') ?>
        <?= $form->field($model, 'fechaBaja') ?>
        <?= $form->field($model, 'fechaNacimiento') ?>
        <?= $form->field($model, 'nombre') ?>
        <?= $form->field($model, 'correo') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- Tenistas-_form -->
