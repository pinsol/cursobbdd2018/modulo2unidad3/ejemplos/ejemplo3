<?php

namespace app\controllers;

use Yii;
use app\models\tenistas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TenistasController implements the CRUD actions for tenistas model.
 */
class TenistasController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all tenistas models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => tenistas::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single tenistas model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


public function actionTodos()
    {
		// realizarlo con activeRecord
		// select * from tenistas;
		$consulta=Tenistas::find()
							->all();
							
		
		// select * from tenistas;
		$consultaArray=Tenistas::find()
							->asArray()
							->all();
		
		
		$datos=Tenistas::find()->all();
		
		
		// select * from tenistas where id in (3,5);
		// $datos1=Tenistas::findAll(3,5);
		
		
        return $this->render('todos', [
            'datos' => $consulta,
			'datos1'=> $consultaArray,
        ]);
    }
	
	public function actionTodos1()
    {
		//variable que apunta a la bbdd
		$conexion=Yii::$app->db;
		
		// SOLO PARA CONSULTAS DE SELECCION
		$datos=$conexion->createCommand("Select * from tenistas")->queryAll();
		
	
        return $this->render('todos1', [
            'datos' => $datos,
        ]);
    }
	
	public function actionConsulta1()
    {
		// select * from tenistas where id=3;
		$resultados=Tenistas::find()
                                    ->where(["id"=>3])
                                        ->all();

							
		
        return $this->render("consultas", [
            "datos" => $resultados,
            "titulo"=>"Consulta1",
            "texto"=>"Mostrar el tenista con id 2",
        ]);
    }
    
    public function actionConsulta2()
    {
		// select * from tenistas order by nombre;
		$resultados=Tenistas::find()
                                    ->orderBy('nombre')
                                        ->all();

							
		
        return $this->render("consultas", [
            "datos" => $resultados,
            "titulo"=>"Consulta2",
            "texto"=>"Mostrar los tenistas ordenados por nombre",
        ]);
    }
    
    public function actionConsulta3()
    {
		// select id, nombre from tenistas;
		$resultados=Tenistas::find()
                                    ->select('id','nombre')
                                        ->all();

							
		
        return $this->render("consultas", [
            "datos" => $resultados,
            "titulo"=>"Consulta3",
            "texto"=>"Mostrar id y el nombre de los tenistas",
        ]);
    }
    
    public function actionConsulta4()
    {
		// select * from tenistas where nombre like "n*" or edad<40;
		$resultados=Tenistas::find()
                                    ->where("nombre like 'n%' or edad<40")
                                        ->all();

							
		
        return $this->render("consultas", [
            "datos" => $resultados,
            "titulo"=>"Consulta4",
            "texto"=>"Mostrar tenistas cuyo nombre empiece con N o cuya edad es menor de 40",
        ]);
    }
	
    /**
     * Creates a new tenistas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		// hemos creado un modelo para trabajar con la bbdd		
        $model = new tenistas();
		
		
		
		//carga los datos del formulario y se lo asigno al modelo
		// $model->load(Yii::$app->request->post());
		// almacenar los datos del modelo en la tabla
		
		// $model->save();
		
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
			
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing tenistas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing tenistas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the tenistas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return tenistas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = tenistas::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
